#include "main.h"
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_gpio.h"
#include "stm32l0xx_hal_gpio_ex.h"
#include "stm32l0xx_hal_rcc.h"

#ifdef TEST
  #define LOOP 
#else
  #define LOOP while(1)
  #include "stm32l0xx.h"
  int main ( void )              
  {
#ifdef TARGET
    return AppMain();
#else
	return 0;
#endif
  }
#endif // TEST

int AppMain( void )
{

//enable 
__HAL_RCC_GPIOA_CLK_ENABLE();

//Enabled Port A Pin 1 as a standard GPIO pin
GPIO_InitTypeDef gpioA_pin0;
gpioA_pin0.Mode = GPIO_MODE_OUTPUT_PP;
gpioA_pin0.Pin = GPIO_PIN_0;
gpioA_pin0.Speed = GPIO_SPEED_MEDIUM;
gpioA_pin0.Pull = GPIO_PULLUP;

HAL_GPIO_Init(GPIOA, &gpioA_pin0);
HAL_Init();

  LOOP
  {
	  GPIOA->ODR ^= GPIO_PIN_0;
	  HAL_Delay(500);
  }
  return 0;
}
