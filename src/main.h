#ifndef MAIN_H
#define MAIN_H

#ifdef TARGET
#include "stm32l0xx_hal.h"
#endif // TARGET

int AppMain( void );

#endif // MAIN_H
